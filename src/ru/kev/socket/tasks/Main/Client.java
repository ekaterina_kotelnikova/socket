package ru.kev.socket.tasks.Main;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Класс, описывающий клиента и выполняющий функции:
 * - принимает текст с клавиатуры
 * - отправляет текст серверу
 * - принимает текст от сервера
 *
 * @author Kotelnikova E.V. group 15it20
 */
public class Client {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             DataInputStream inputStream = new DataInputStream(socket.getInputStream());
             DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream())) {

            while(true){
                System.out.println("Пожалуйста, введите слово: ");
                String request = scanner.nextLine();
                outputStream.writeUTF(request);
                outputStream.flush();
                System.out.println("отправлено серверу: " + request);

                if (request.equals("stop")) {
                    System.out.println("");
                    break;
                }
                String response = inputStream.readUTF();
                System.out.println("прислал сервер: " + response);
                Thread.sleep(2000);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
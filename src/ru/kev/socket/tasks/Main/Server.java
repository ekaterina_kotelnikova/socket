package ru.kev.socket.tasks.Main;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Класс, описывающий сервер и выполняющий функции:
 * - ожидания клиента
 * - приём сообщения от клиента
 * - передача клиенту его же сообщения
 *
 * @author Kotelnikova E.V. group 15it20
 */

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Сервер ждёт клиента...");

        try (Socket clientSocket = serverSocket.accept();
             DataInputStream inputStream = new DataInputStream(clientSocket.getInputStream());
             DataOutputStream outputStream = new DataOutputStream(clientSocket.getOutputStream())) {

            System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());
            String response;
            while ((response = inputStream.readUTF()) != null) {
                System.out.println("прислал клиент: " + response);
                outputStream.writeUTF(response);
                outputStream.flush();
                if (response.equals("stop")) {
                    break;
                }
                System.out.println("Отправлено клиенту: " + response);
                Thread.sleep(2000);
            }
            System.out.println("клиент отключился");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
